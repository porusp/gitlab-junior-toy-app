require 'grape'

module API
  module Entities
    class User < Grape::Entity
      expose :id, documentation: { type: 'integer', example: 1 }
      expose :name, documentation: { type: 'string', example: 'Administrator' }
      expose :email, documentation: { type: 'string', example: 'email@example.com' }
      expose :admin, documentation: { type: 'boolean', example: true }
      expose :web_url, documentation: { type: 'string', example: 'https://example.com/users/1' } do |user|
        GitlabJunior::Application.routes.url_helpers.user_url(user)
      end
    end
  end
end
