module QA
  module Page
    module PersonalAccessToken
      class Show < Base
        def fill_name_field(email)
          fill_element(:name_field, email)
        end

        def click_generate_button
          click_element(:generate_button)
        end

        def latest_token
          all_elements(:personal_access_token_text, minimum: 1).first.text
        end
      end
    end
  end
end
