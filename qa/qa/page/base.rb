require 'capybara/dsl'

module QA
  module Page
    class Base
      include Capybara::DSL

      def self.perform
        yield new if block_given?
      end

      def selector_css(element_name)
        %Q([data-qa-selector="#{element_name.to_s}"])
      end

      def click_element(name, **kwargs)

        wait = kwargs.delete(:wait) || Capybara.default_max_wait_time
        text = kwargs.delete(:text)

        find(selector_css(name), text: text, wait: wait).click
      end

      def fill_element(name, content, **kwargs)
        wait = kwargs.delete(:wait) || Capybara.default_max_wait_time

        find(selector_css(name), wait: wait).set(content)
      end

      def has_text?(text, wait: Capybara.default_max_wait_time)
        page.has_text?(text, wait: wait)
      end

      def all_elements(name, **kwargs)
        all_args = [:minimum, :maximum, :count, :between]

        if kwargs.keys.none? { |key| all_args.include?(key) }
          raise ArgumentError, "Please use :minimum, :maximum, :count, or :between so that all is more reliable"
        end

        all(selector_css(name), **kwargs)
      end

      def has_element?(name, **kwargs)
        text = kwargs.delete(:text)
        klass = kwargs.delete(:class)
        visible = kwargs.delete(:visible)
        wait = kwargs.delete(:wait) || Capybara.default_max_wait_time

        has_css?(selector_css(name), text: text, wait: wait, class: klass, visible: visible)
      rescue Capybara::ElementNotFound
        false
      end

      def has_no_element?(name, **kwargs)
        wait = kwargs.delete(:wait) || Capybara.default_max_wait_time
        text = kwargs.delete(:text)

        has_no_css?(selector_css(name), wait: wait, text: text)
      end

      def within_element(name, **kwargs, &block)
        text = kwargs.delete(:text)

        page.within(selector_css(name), text: text, &block)
      end
    end
  end
end
