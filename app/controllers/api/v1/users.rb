module API
  module V1
    class Users < Grape::API
      include API::V1::Defaults

      resource :users do
        desc 'Return all users' do
          success Entities::User
        end
        params do
          optional :email, type: String, desc: 'Email address of user'
        end
        get do
          users = params[:email].present? ? User.where(email: params[:email]) : User.all
          present users, with: Entities::User
        end

        desc 'Return a user'
        params do
          requires :id, type: Integer, desc: 'ID of the user'
        end
        get ':id' do
          user = User.where(id: params[:id]).first!
          present user, with: API::Entities::User
        end

        desc 'Delete a user'
        params do
          requires :id, type: Integer, desc: 'ID of the user'
        end
        delete ':id' do
          authenticated_as_admin!

          User.find(params[:id]).destroy
          status 204
        end

        desc 'Create a user'
        params do
          requires :email, type: String, desc: 'The email of the new user'
          requires :name, type: String, desc: 'The name of the new user'
          requires :password, type: String, desc: 'The password of the new user'
          requires :password_confirmation, type: String, desc: 'The password of the new user again'
        end
        post do
          authenticated_as_admin!

          params = permitted_params

          user = User.new(params)
          if user.save
            present user, with: API::Entities::User
          else
            render_validation_error!(user)
          end
        end
      end
    end
  end
end
