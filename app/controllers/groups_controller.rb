class GroupsController < ApplicationController
  before_action :logged_in_user, only: [:create, :edit, :update, :destroy]
  before_action :allowed_user, only: [:create, :edit, :update, :destroy]

  def new
    @new_group = current_user.groups.new
  end

  def edit
    @group = Group.find(params[:id])
  end

  def update
    @group = Group.find(params[:id])
    if @group.update(group_params)
      flash[:success] = 'Group updated'
      redirect_to @group
    else
      render 'edit', status: :unprocessable_entity
    end
  end

  def create
    @new_group = current_user.groups.build(group_params)
    if @new_group.save
      flash[:success] = "Group created!"
      redirect_to @new_group
    else
      render 'new', status: :unprocessable_entity
    end
  end

  def show
    @group = Group.find(params[:id])
    @sub_groups = @group.sub_groups.paginate(page: params[:page])
  end

  def destroy
    Group.find(params[:id]).destroy
    flash[:success] = "Group deleted"
    if request.referrer.nil?
      redirect_to root_url, status: :see_other
    else
      redirect_to request.referrer, status: :see_other
    end
  end

  private

  def group_params
    params.require(:group).permit(:name, :description, :owner_id, :parent_group_id)
  end

  def allowed_user
    return unless params[:id]
    return if params[:id] && (current_user?(Group.find(params[:id]).owner) || current_user.admin?)

    flash[:danger] = "You are not allowed to perform this action"
    redirect_to(root_url, status: :unauthorized)
  end
end
